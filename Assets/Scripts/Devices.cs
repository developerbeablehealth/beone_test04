﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Threading.Tasks;

using Firebase.Storage;
using System.IO;

public class Devices : MonoBehaviour
{

    [SerializeField] GameObject DevicesScrollview;
    [SerializeField] GameObject profilepanel;
    [SerializeField] GameObject profilebar;

    public bool statusBar;
    public AndroidStatusBar.States states = AndroidStatusBar.States.Visible;
    public Text userName;
    static Firebase.Auth.FirebaseAuth auth;
    public Image loadProfilePhoto;
    public Image profilePanelPic;
    public Text profilePanelCoins;

    void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidStatusBar.dimmed = !statusBar;
            AndroidStatusBar.statusBarState = states;
        }
        DownloadProfilePhoto();
    }

    public void Start()
    {

        Screen.fullScreen = false;
        profilepanel.SetActive(false);
        profilebar.SetActive(false);
        userName.text = Login.userName;

    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !(profilepanel.activeInHierarchy == true) )
        {
            // profilepanel.SetActive(false);
           
            SceneManager.LoadScene("SignIn");
            
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && (profilepanel.activeInHierarchy == true))
        {

            SceneManager.LoadScene("Devices");
        }
        
      

       
    } 

    public void Armablebtn()
    {
        SceneManager.LoadScene("Dashboard");
    }

    public void profilebtn()
    {
       
        profilepanel.SetActive(true);
        profilebar.SetActive(true);
        DevicesScrollview.SetActive(false);

       

    }


    public void backbtn()
    {
        SceneManager.LoadScene("Devices");
    }

    public void Homebtn()
    {
        DevicesScrollview.SetActive(true);

        profilepanel.SetActive(false);
       
    }

    public void Testyourselfbtn()
    {
        SceneManager.LoadScene("TestYourselfscene");
    }

    public void MessagePanelbtn()
    {
        SceneManager.LoadScene("Message_1");
    }

    public void NotificationPanelbtn()
    {
        SceneManager.LoadScene("Notfication");
    }

    public void HomePanelbtn()
    {
        SceneManager.LoadScene("Devices");
    }

    public void Analysisbtn()
    {
        SceneManager.LoadScene("Analysis");
    }


    public void SignOut()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        Debug.Log("user signed out and removed from remember-me option , need to enter credentials on the next app open");
        PlayerPrefs.SetInt("rememberUserLogin", 0);

        auth.SignOut();
        SceneManager.LoadScene("SignIn");
    }

    FirebaseStorage storage;
    StorageReference storage_ref;
    private Texture2D target1;

    public static Sprite loadUserPic;
    public void DownloadProfilePhoto()
    {
        

        var dpPath = Application.persistentDataPath + "/profilePic.png";
        storage = FirebaseStorage.DefaultInstance;
        storage_ref = storage.GetReferenceFromUrl("gs://beone-284914.appspot.com/users/warrior/"+Login.userUID+"/profilePhoto.png");



        // Download to the local filesystem
        storage_ref.GetFileAsync(dpPath).ContinueWith(task => {
            if (!task.IsFaulted && !task.IsCanceled)
            {
                Debug.Log("File downloaded.");
            }
        });



        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(dpPath))
        {
            fileData = File.ReadAllBytes(dpPath);
            tex = new Texture2D(265, 265);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            Sprite sprite = Sprite.Create(tex,
           new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            loadProfilePhoto.sprite = sprite;
            loadUserPic = sprite;
            profilePanelPic.sprite = sprite;
            profilePanelCoins.text = Login.totalCoins.ToString();
        }
       // return tex;
        ////////////

       /* int height = 265;
        int width = 265;
        target1 = new Texture2D(height, width, TextureFormat.RGB24, false);

        WWW www = new WWW(dpPath); //this is not working on the android device
        //Texture2D texture = new Texture2D(1, 1);
        www.LoadImageIntoTexture(target1);

        Sprite sprite = Sprite.Create(target1,
            new Rect(0, 0, target1.width, target1.height), Vector2.zero);

        //myImage.SetNativeSize();
        //loadProfilePhoto.SetNativeSize();*/
      /*  loadProfilePhoto.sprite = sprite;
        loadUserPic = sprite;*/


    }






}
