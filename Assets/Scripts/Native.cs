﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Native : MonoBehaviour
{
    public Image profileImage;
    public static Image _Dp;
    public static string profilePhotoPath;
    public Text deviceDpPath;
    public Text photoStorePath;
    Texture2D test;

    // Start is called before the first frame update
    public void Image()
    {

        NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                profilePhotoPath = path;
                deviceDpPath.text = path;
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, 265);
                test = texture;
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

               

                profileImage.sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);

                _Dp = profileImage;
                var Bytes = profileImage.sprite.texture.EncodeToPNG();
                //Debug.Log("Captured image is " + texture);
                PlayerPrefs.SetString("profilePic", Bytes.ToString());


                string path2 = Application.persistentDataPath + "/profilePhoto.png";
                photoStorePath.text = path2;

                File.WriteAllBytes(path2, Bytes);

            }
        }, "Select png image ", "image/*", 265);


        
    }

    public void test01()
    {

        

    }


    public void OnChangePhotoButtonClick()
    {
        // Don't attempt to pick media from Gallery/Photos if
        // another media pick operation is already in progress
        if (NativeGallery.IsMediaPickerBusy())
            return;

        // Pick a PNG image from Gallery/Photos
        // If the selected image's width and/or height is greater than 512px, down-scale the image

    }

    // Update is called once per frame
    void Update()
    {

    }
}
